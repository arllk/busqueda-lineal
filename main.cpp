// El codigo esta en https://gitlab.com/arllk/busqueda-lineal

#include <iostream>
using namespace std;

/** Funcion de busqueda
 *
 * arr -> es el arreglo en el que se va a realizar la busqueda
 * array_tammano -> el tamaño del arreglo
 * numero_a_buscar -> el numero a buscar
 *
 * Retorna el indice en el que se encuentra en el array el numero a buscar o
 * -1 si no encuentra nada
 *
 * En estos tipos de funciones para evitar tener que devolver un bool para cuando
 * no se encuentra el resultado se devuelve un numero "absurdo", que es aquel que
 * tenemos la certeza que jamas va a devolver la funcion en funcionamiento correcto
 *
 */
int buscar(int arr[], int array_tammano, int numero_a_buscar)
{
    // Se crea la variable del indice que vamos a devolver si encontramos la ubicacion
    int indice;

    // Entrando al loop a buscar de la manera mas basica
    for (indice = 0; indice < array_tammano; indice++){

		// Comparando el numero del array que esta en el indice con el numero a buscar
        if (arr[indice] == numero_a_buscar) {

        	// Bueno si llegamos aca es que el numero en este indice es el mismo del que buscamos
        	// Y aca estamos retornando ese indice y saliendo de la funcion
            return indice;
        }
    }

    // Esto se ejecuta solo si el numero nunca se encuentra, asi que podemos decir que ese numero no
    // existe en el arreglo asi que devolvemos ese numero absurdo (-1)
    return -1;
}

int main()
{
	// Declarando el arreglo
    int arr[] = { 2, 3, 4, 10, 40 };

    // Definiendo el numero que se quiere encontrar
    int numero_que_quiero = 10;

    // Obteniendo el tamaño del arreglo
    int tamanno_arreglo = sizeof(arr) / sizeof(arr[0]);

    // solicitamos la busqueda con el arreglo, el tamaño del arreglo y el numero a buscar y lo almacenamos
    // en resultado
    int resultado = buscar(arr, tamanno_arreglo, numero_que_quiero);

    // Verificamos si el resultado es ese numero absurdo que solo retorna si es que no hay el elemento
    if (resultado == -1){
		cout<<"El numero buscado :"<<numero_que_quiero<<" no esta en el arreglo :(";
    }
    // Pero si es que el valor del resultado no es -1 sabemos que existe el numero que buscamos, y ahora tenemos el
    // indice o posicion en la que esta (ahora podemos usar ese indice para referirnos rapidamente al numero)
    else{
		cout<<"El numero fue encontrado en el indice: " <<resultado;
    }

    return 0;
}
